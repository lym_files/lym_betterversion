package main

import (
	"context"
	"fmt"
	controller "golangJquerry/mongoFiles"
	"html/template"
	"log"
	"net/http"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var templates *template.Template
var connectCollection *mongo.Collection
var connectCollection2 *mongo.Collection
var connectCollection3 *mongo.Collection
var connectCollection4 *mongo.Collection

func main() {

	local := time.Now()
	zone, offset := local.Zone()
	layout24 := "2006-01-02 15:04:05"
	layout12 := "2006-01-02 03:04:05 PM"

	fmt.Println("\nLocal time (24-hour format):", local.Format(layout24))
	fmt.Println("Local time (12-hour format):", local.Format(layout12))
	fmt.Println("Timezone:", zone)
	fmt.Println("Timezone offset:", offset/60/60)

	mongoCollection, _, _ := connectMongo()
	_, _, pixCollection := connectMongo()
	lymCollection := connectMongo2()

	templates = template.Must(template.ParseGlob("HTML_Files/*.html"))

	controller.HandleHome(templates)
	controller.AddUserMongo(mongoCollection, templates)
	controller.View(mongoCollection, pixCollection, lymCollection, templates)
	controller.GeneratePDFHandler(lymCollection)
	controller.GeneratePDFHandlerAll(lymCollection)
	controller.Delete(mongoCollection, templates)
	controller.AddUserLYMMongo(lymCollection, templates)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func connectMongo() (*mongo.Collection, *mongo.Collection, *mongo.Collection) {

	clientOptions := options.Client().ApplyURI("mongodb+srv://virgodosal:admin123@zuitt-bootcamp.izbumtb.mongodb.net/?retryWrites=true&w=majority")
	client, err := mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(context.Background(), nil)
	if err != nil {
		log.Fatal(err)
	}
	connectCollection = client.Database("Chat").Collection("Users")
	connectCollection2 = client.Database("Chat").Collection("Product")
	connectCollection3 = client.Database("Document").Collection("User")

	fmt.Printf("\nConnected")
	return connectCollection, connectCollection2, connectCollection3
}
func connectMongo2() *mongo.Collection {

	clientOptions := options.Client().ApplyURI("mongodb://admin:pa55w0rd@192.168.63.230:27017/?")
	client, err := mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(context.Background(), nil)
	if err != nil {
		log.Fatal(err)
	}
	connectCollection4 = client.Database("LYM").Collection("Users")

	fmt.Printf("\nConnected to LYM database\n")
	return connectCollection4
}
