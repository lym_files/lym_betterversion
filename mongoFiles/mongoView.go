package controller

import (
	"context"
	"encoding/base64"
	"fmt"
	"github.com/jung-kurt/gofpdf"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"strings"
)

type User struct {
	ID    primitive.ObjectID `json:"_id" bson:"_id,omitempty"`
	Name  string             `json:"name"`
	Age   int                `json:"age"`
	Email string             `json:"email" bson:"email" validation:"required"`
	Image string             `json:"image"`
}

type Pix struct {
	ID        primitive.ObjectID `json:"_id" bson:"_id,omitempty"`
	Name      string             `json:"name"`
	Age       int                `json:"age"`
	Email     string             `json:"email" bson:"email" validation:"required"`
	Image     string             `json:"image"`
	ImageName string             `json:"imagename"`
	File      string             `json:"file"`
	Filename  string             `json:"filename"`
}

type RowData struct {
	Name     string
	Age      string
	Email    string
	Filename string
}

func View(mongoCollection *mongo.Collection, pixCollection *mongo.Collection, lymCollection *mongo.Collection, templates *template.Template) {

	http.HandleFunc("/viewUsers", func(w http.ResponseWriter, r *http.Request) {

		if r.Method == "GET" {
			// [START OF DATABASE "CHAT" COLLECTION "USERS"]
			cursor, err := mongoCollection.Find(context.Background(), bson.D{})
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			defer cursor.Close(context.Background())
			fmt.Printf("%v", cursor)

			var users []User

			for cursor.Next(context.Background()) {
				var user User
				if err := cursor.Decode(&user); err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				users = append(users, user)
			}

			if err := cursor.Err(); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			// [END OF DATABASE "CHAT" COLLECTION "USERS"]

			// [START OF DATABASE "DOCUMENT" COLLECTION "USERS"]
			var images []Pix
			cursor2, err := pixCollection.Find(context.Background(), bson.M{})
			if err != nil {
				log.Fatal(err)
			}
			defer cursor2.Close(context.Background())

			for cursor2.Next(context.Background()) {
				var image Pix
				err := cursor2.Decode(&image)
				if err != nil {
					log.Fatal(err)
				}

				img := image.Image
				//str := string(img)
				//	byteSlice := []byte(img)
				//	fmt.Fprintf("%v", byteSlice)
				base64Str := base64.StdEncoding.EncodeToString([]byte(img))
				//fmt.Printf("%v", base64Str)
				image.Image = base64Str

				images = append(images, image)

			}

			if err := cursor2.Err(); err != nil {
				log.Fatal(err)
			}

			// [END OF DATABASE "DOCUMENT" COLLECTION "USERS"]

			// [START OF DATABASE "DOCUMENT" COLLECTION "USERS"]

			nameSearch := r.FormValue("nameSearch")
			filter := bson.M{}
			fmt.Printf("\nName: %v\n", nameSearch)

			if nameSearch != "" {
				filter["name"] = bson.M{"$regex": primitive.Regex{Pattern: nameSearch, Options: "i"}}
			}
			var images2 []Pix
			cursor3, err := lymCollection.Find(context.Background(), filter)
			if err != nil {
				log.Fatal(err)
			}
			defer cursor3.Close(context.Background())

			for cursor3.Next(context.Background()) {

				var image2 Pix

				err := cursor3.Decode(&image2)
				if err != nil {
					log.Fatal(err)
				}

				img2 := image2.Image
				fileExcel := image2.File
				byteSlice := []byte(img2)
				//	byteSliceExcel := string(image2.File)
				//	fmt.Printf("\n: %v\n", fileExcel)

				imageMimeType := http.DetectContentType(byteSlice)
				//	imageMimeTypExcel := http.DetectContentType(byteSliceExcel)
				//	fmt.Printf("%v", imageMimeTypExcel)

				// Check if the image is a PNG file
				if imageMimeType == "image/png" {
					fmt.Printf("\n%v is PNG type image\n", image2.ImageName)
				} else if imageMimeType == "image/jpeg" {
					fmt.Printf("\n%v is JPG type image\n", image2.ImageName)
				} else if imageMimeType == "image/gif" {
					fmt.Printf("\n%v is GIF type image\n", image2.ImageName)
				}

				base64StrFile := base64.StdEncoding.EncodeToString([]byte(fileExcel))
				image2.File = base64StrFile

				base64Str := base64.StdEncoding.EncodeToString([]byte(img2))
				image2.Image = base64Str

				images2 = append(images2, image2)

			}

			if err := cursor2.Err(); err != nil {
				log.Fatal(err)
			}

			// [END OF DATABASE "DOCUMENT" COLLECTION "USERS"]

			data := struct {
				Users     []User
				Pictures  []Pix
				Pictures2 []Pix
			}{
				Users:     users,
				Pictures:  images,
				Pictures2: images2,
			}

			err = templates.ExecuteTemplate(w, "view.html", data)

			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

		} else {
			http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		}

	})

}

func GeneratePDFHandler(lymCollection *mongo.Collection) {
	http.HandleFunc("/generatepdf", func(w http.ResponseWriter, r *http.Request) {

		id := r.URL.Query().Get("id")
		name := r.URL.Query().Get("name")
		age := r.URL.Query().Get("age")
		email := r.URL.Query().Get("email")
		image := r.URL.Query().Get("iName")

		pdf := gofpdf.New("P", "mm", "Letter", "")
		pdf.AddPage()
		pdf.SetFont("Arial", "B", 8)

		header := []string{"ID", "Name", "Age", "Email", "Image Name"}

		cellWidth := 38.0
		cellHeight := 10.0
		textColor := 255

		pdf.SetFillColor(200, 200, 200)
		pdf.SetTextColor(0, 0, 0)

		for _, column := range header {
			pdf.CellFormat(cellWidth, cellHeight, column, "1", 0, "C", true, 0, "")
		}

		pdf.Ln(-1)

		data := [][]string{
			{id, name, age, email, image},
		}

		for _, row := range data {
			for _, cell := range row {
				pdf.CellFormat(cellWidth, cellHeight, cell, "1", 0, "C", false, 0, "")
			}
			pdf.Ln(-1)
		}

		pdf.SetFont("Arial", "", 8)
		pdf.SetFillColor(255, 255, 255)
		pdf.SetTextColor(textColor, textColor, textColor)

		err := pdf.Output(w)
		if err != nil {
			http.Error(w, "Failed to generate PDF", http.StatusInternalServerError)
			return
		}
	})
}
func GeneratePDFHandlerAll(lymCollection *mongo.Collection) {
	http.HandleFunc("/generatepdfAll", func(w http.ResponseWriter, r *http.Request) {
		values := r.URL.Query().Get("all")
		//fmt.Printf("\nValue: %v\n", values)

		pdf := gofpdf.New("P", "mm", "A4", "")
		pdf.AddPage()

		pdf.SetFont("Arial", "", 10)
		pdf.SetFillColor(200, 200, 200)

		headers := []string{"ID", "Name", "Age", "Email", "Image", "ImageName", "Excel"}
		cellWidths1 := []float64{20, 33, 20, 35, 27, 27, 27}
		cellHeight := 15.0

		for i, header := range headers {

			pdf.SetFillColor(200, 200, 200)
			pdf.CellFormat(cellWidths1[i], cellHeight, header, "1", 0, "C", true, 0, "")
		}

		pdf.Ln(cellHeight)

		rowy := strings.Split(values, ",")
		cellWidths := []float64{20, 33, 20, 35, 27, 27, 27}
		counter := 0
		counter2 := 0
		counter3 := 0
		counter4 := 0
		var tempNum int
		var tempName string

		for _, row := range rowy {
			rows := strings.Split(row, ",")
			for _, cell := range rows {
				pdf.SetFillColor(50, 50, 0)
				if counter == 0 {
					tempNum = counter2 + 1
					cell = strconv.Itoa(tempNum)
					counter2++
				}

				if counter == 4 {
					tempNum = counter4 + 1
					tempName = "Image" + strconv.Itoa(tempNum)
					cell = tempName
					//fmt.Printf("Excel name: %v\n", cell)
					counter4++
				}

				if counter == 6 {
					tempNum = counter3 + 1
					tempName = "Excel" + strconv.Itoa(tempNum)
					cell = tempName
					//fmt.Printf("Excel name: %v\n", cell)
					counter3++
				}
				pdf.CellFormat(cellWidths[counter], 10, cell, "1", 0, "C", false, 0, "")
				counter++

				if counter == 7 {
					pdf.Ln(10)
					counter = 0
				}
			}
		}

		w.Header().Set("Content-Disposition", "attachment; filename=generated_pdf.pdf")
		w.Header().Set("Content-Type", "application/pdf")
		if err := pdf.Output(w); err != nil {
			http.Error(w, "Failed to generate PDF", http.StatusInternalServerError)
		}
	})
}
