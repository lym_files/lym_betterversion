package controller

import (
	"html/template"
	"net/http"
)

func HandleHome(templates *template.Template) {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		templates.ExecuteTemplate(w, "jquerry.html", nil)

	})
}
