package controller

import (
	"context"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Document struct {
	ID        primitive.ObjectID `json:"_id" bson:"_id,omitempty"`
	Name      string
	Age       int
	Email     string
	Image     []byte
	ImageName string
	File      []byte
	Filename  string
}

func AddUserLYMMongo(lymCollection *mongo.Collection, templates *template.Template) {
	http.HandleFunc("/addLYMuser", func(w http.ResponseWriter, r *http.Request) {

		if r.Method == "POST" {

			name := r.FormValue("Name")
			ageStr := r.FormValue("Age")
			email := r.FormValue("Email")
			image, header, err := r.FormFile("Image")
			if err != nil {
				log.Println(err)
				http.Error(w, "Bad Request", http.StatusBadRequest)
				return
			}
			defer image.Close()
			file, header2, _ := r.FormFile("File")
			defer file.Close()
			//
			/* fileData, err := ioutil.ReadAll(file)
			if err != nil {
				log.Println(err)
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			} */

			fileData, err := ioutil.ReadAll(file)
			if err != nil {
				log.Println(err)
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			}

			filename := header2.Filename

			newFile, err := os.Create(filename)
			if err != nil {
				log.Println(err)
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			}
			defer newFile.Close()

			_, err = newFile.Write(fileData)
			if err != nil {
				log.Println(err)
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			}

			log.Println("File saved:", filename)
			//
			filenameImage := header.Filename

			age, err := strconv.Atoi(ageStr)
			if err != nil {
				log.Println(err)
				http.Error(w, "Bad Request", http.StatusBadRequest)
				return
			}

			imageData, err := ioutil.ReadAll(image)
			if err != nil {
				log.Println(err)
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			}

			doc := Document{
				Name:      name,
				Age:       age,
				Email:     email,
				Image:     imageData,
				ImageName: filenameImage,
				File:      fileData,
				Filename:  filename,
			}
			fmt.Println("Successfull added")

			_, err = lymCollection.InsertOne(context.Background(), doc)

		} else {
			fmt.Println("")
		}
		templates.ExecuteTemplate(w, "addLymUser.html", nil)

	})
}
