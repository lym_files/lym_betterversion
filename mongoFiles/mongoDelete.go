package controller

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"html/template"
	"net/http"
)

func Delete(mongoCollection *mongo.Collection, templates *template.Template) {
	http.HandleFunc("/view", func(w http.ResponseWriter, r *http.Request) {

		cursor, err := mongoCollection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())
		var users []User
		for cursor.Next(context.Background()) {
			var user User
			if err := cursor.Decode(&user); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			users = append(users, user)
		}
		if err := cursor.Err(); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var userID = r.FormValue("deleteData")
		fmt.Println("\n", userID)

		objectID, err := primitive.ObjectIDFromHex(userID)
		fmt.Printf("ObjectID: %v\n", objectID)

		filter := bson.M{"_id": objectID}
		result, err := mongoCollection.DeleteOne(context.Background(), filter)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		fmt.Printf("result: %v", result)

		templates.ExecuteTemplate(w, "delete.html", users)
	})
}
