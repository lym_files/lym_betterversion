package controller

import (
	"context"
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"go.mongodb.org/mongo-driver/mongo"
)

func AddUserMongo(mongoCollection *mongo.Collection, templates *template.Template) {
	http.HandleFunc("/addMuser", func(w http.ResponseWriter, r *http.Request) {

		if r.Method == "POST" {
			name := r.FormValue("Name")
			ageStr := r.FormValue("Age")
			email := r.FormValue("Email")
			imageLink := r.FormValue(("ImageURL"))

			fmt.Printf("\n%v", name)
			fmt.Printf("\n%v", ageStr)
			fmt.Printf("\n%v", email)
			fmt.Printf("\n%v", imageLink)

			if name != "" || ageStr != "" || email != "" || imageLink != "" {

				age, err := strconv.Atoi(ageStr)
				if err != nil {
					http.Error(w, "Invalid age", http.StatusBadRequest)
					return
				}

				user := User{
					Name:  name,
					Age:   age,
					Email: email,
					Image: imageLink,
				}
				fmt.Printf("%v", user)

				_, err = mongoCollection.InsertOne(context.Background(), user)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

			} else {
				fmt.Println("")
			}
		} else {
			fmt.Println("")
		}

		templates.ExecuteTemplate(w, "addUser.html", nil)

	})

}
